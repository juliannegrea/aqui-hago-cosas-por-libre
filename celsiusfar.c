#include <stdio.h>

/*Imprimir una tabla de equivalencias dentre Celsius y Fahrneheit
	la tablla va de 20 en 20 desde 0 hasta 500*/



int main()
{

	float fahr, celsius;

	int lower, upper, step;

	lower = 0;
	upper = 500; 
	step = 20 ;


	celsius = lower;

	printf("Fahrenheit\tGrados Celsius\n");

	while (celsius <= upper) {

		fahr = (celsius / 5 * 9) + 32;
        printf("%3.0f\t\t%6.1f\n", celsius, fahr );
        celsius = celsius + step ;
    }

		return 0;


}
