#include <stdio.h>


int main()
{
	int c , nl;

	nl = 0;

	while((c = getchar()) != EOF)
		if (c == '\n')

			++nl;
		printf("%d\n", nl );


	return 0;
}



/*FUNCIONAMIENTO 

1. DECLARAMOS LAS VARIABLES C nl(numero lineas)
2. Queremos contar desde cero nl=0;
3. Usamos ESTRUCTURA DE CONTROL ANIDADA (estructura control while,for + condicion if, else, ifelse
4. Una vez que la condicion sea cierta dentro del if que cuando se encuentra \n (salto linea) 
vaya aumentando en 1 el numero de lineas 
5 Seguidamente vamos a imprimir los numero que vayan a contar de tipo entero suimple */