#include <stdio.h>

/*Imprimir una tabla de equivalencias dentre Fahrenheint y Celsius
	la tablla va de 20 en 20 desde 0 hasta 300*/


int main()
{
	float fahr, celsius;						/*declaramos al principio fahr celsius como tipo de dato entero
													pero lo cambiamos por float para usar numero con coma 
													a continuacion llamamos las variables lower numero mas pequeño y upper como mas grande para usar
													step para el paso de 20 en 20 */
	int lower, upper, step;

	lower = 0;
	upper = 300; 
	step = 20 ;


	fahr = lower;								/*definimos el valor de fahr */

	printf("Fahrenheit \t Grados Celsius \n");	/*lo hicimos al final del programa para llamar a cada una de las columnas */

	while (fahr <= upper) {					   /*usamos la estuctura de contrlos en este caso while para ejecutar el bloque varias 
												hasta que fahr sea menor o igual a 300 si llega a 320 se deja de ejecutar */

		celsius = 5 * (fahr-32) / 9;		   /*aqui aplicamos la formula de la conversion y guardamos en resultado 
												en la variable celsius*/

		printf("%3.0f\t\t\t%6.1f\n", fahr, celsius ); /*anteriormente usamos %d para tipos de datos enteros pero 
		cambiamos por % de float para usar numeros con coma y poner las justificaciones que deseamos 
		completando con 3 caracteres en total y 0 decimales y 6 caracteres y 1 decimal*/

		 fahr = fahr + step ; 		/*aqui lo que hacemos es aumentar por cada vez que se 
		 							comprueba condicion con resultado true aumentar fahr 20 que 
		 							es lo que hemos declarado que vale al principo */

	}


	return 0;
}